#ifndef PTHREAD_WORK_H_
#define PTHREAD_WORK_H_

#include "pthread_init.h"

void *pthread_work(void *args);

#endif

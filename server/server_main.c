
/*
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
*/
#include "server.h"


int main(int argc,char **argv)
{

	int		server_port;
	int		sockfd 	= -1;
	int		clienfd	= -1;
	int		re_val 	= -1;

	pthread_t 	pthread_id;

	struct sockaddr_in   clien_addr;
	socklen_t            clienaddr_len;

	//pthread_args_t       pthread_args ;
	pthread_args_t       *pthread_args ;

	//参数解析
	re_val = argument_parse(argc, argv, &server_port);
	if(re_val < 0)
	{
		printf("参数解析错误!\n");
		exit(0);
	}

	//套接字初始化
	re_val = socket_init(server_port,&sockfd);
	if(re_val < 0)
	{
		printf("套接字初始化错误!\n");
	}

	//服务器开始工作
	while(1)
	{	
		//等待客户端连接上来
		printf("serve is waitting to connect new client...\n");
		mylog(__FUNCTION__,__LINE__,INFO,"serve is waitting to connect new client...:%s\n",strerror(errno));

		if((clienfd = accept(sockfd,(struct sockaddr *)&clien_addr,&clienaddr_len)) <0 )
		{

			printf("serve accept failed: %s\n",strerror(errno));
			mylog(__FUNCTION__,__LINE__,ERROR,"server accept faild:%s\n",strerror(errno));
			return -1;
		
		}
		printf("Accept new client[%s:%d] scuccessfully.\n",inet_ntoa(clien_addr.sin_addr),ntohs(clien_addr.sin_port));
		mylog(__FUNCTION__,__LINE__,INFO,"Accept new client[%s:%d] scuccessfully.\n",\
			inet_ntoa(clien_addr.sin_addr),ntohs(clien_addr.sin_port));


		//线程开始	
		pthread_args = (pthread_args_t *)malloc(sizeof(pthread_args_t));
		//pthread_args.clienfd = clienfd;
		pthread_args->clienfd = clienfd;	
                                       
 		thread_start(&pthread_id, pthread_work, pthread_args);

	}
	

	return 0;
}

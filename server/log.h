#ifndef LOG_H_
#define LOG_H_

/*定义日志等级*/
enum LogLevel
{
	ERROR = 1,
	WARN  = 2,
	INFO  = 3,
	DEBUG = 4,
};

int mylog(const char *function, int line, enum LogLevel level, const char *fmt, ...);

#endif

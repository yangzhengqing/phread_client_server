/*================================================================
*   Copyright (C) 2020 belonging to YangZhengQing. All rights reserved.
*   
*   文件名称：phread_init.h
*   创 建 者：YangZhengQing
*   创建日期：2020年02月20日
*   描    述：
*
*================================================================*/


#pragma once

#ifndef PTHREAD_INIT_H_
#define PTHREAD_INIT_H_

#include <pthread.h>

/*定义子线程参数结构体*/
typedef struct
{
	int		clienfd;//客户端文件描述符
	int		count;//线程数
	pthread_mutex_t	lock;//线程锁

}pthread_args_t;

/*定义一个函数类型*/
typedef void *(THREAD_BODY) (void *thread_arg);

/*声明功能函数*/
int pthread_init(pthread_attr_t *thread_attr,pthread_args_t *thread_args);
int thread_start(pthread_t * thread_id, THREAD_BODY * thread_workbody, void *pthread_args);
#endif

/*
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<getopt.h>
*/
#include"server.h"


void print_usage(char *program)
{
	printf("%s usages: %s\n",program,strerror(errno));
	printf("-p(port):serve listen port\n");
	printf("-h(help):help information\n");
}

/*功能：命令行参数解析
 * argc；参数个数
 * argv：参数
 * serv_port：服务器端口
 */
int argument_parse(int argc,char **argv,int *serv_port)
{
	int               opt;

	struct option long_options[]=
	{
		{"port",1,NULL,'p'},
		{"h",0,NULL,'h'},
		{0,0,0,0}
	};

	while((opt = getopt_long(argc,argv,"p:h",long_options,NULL)) > 0)
	{
		switch(opt)
		{
			case 'p':
				*serv_port = atoi(optarg);
				break;	
			case 'h':
				print_usage(argv[0]);
				return -1;
				
		
			default:
				print_usage(argv[0]);
				return -1;
				
		}      
	}

	if(*serv_port == 0)
	{
		print_usage(argv[0]);
		return -1;
	}

	return 0;
}

#ifndef SERVER_H_
#define SERVER_H_
//库文件
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include<getopt.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <pthread.h>

//用户文件
#include "server_opt.h"
#include "server_init.h"
#include "log.h"
#include "database.h"
#include "deal_data.h"
#include "pthread_init.h"
#include "pthread_work.h"


#endif
